import roleBuilder from "roles/builder";
import roleHarvester from "roles/harvester";
import roleMaintainer from "roles/maintainer";
import { drawHex } from "utils";
import { ErrorMapper } from "utils/ErrorMapper";

const NEEDED_ROLES: Map<string, number> = new Map();
NEEDED_ROLES.set("harvester", 3);
NEEDED_ROLES.set("maintainer", 2);
NEEDED_ROLES.set("builder", 3);
NEEDED_ROLES.set("advancedHarvester", 10);
NEEDED_ROLES.set("advancedMaintainer", 10);
const CREEP_TEMPLATES: CreepTemplates = {
  advancedHarvester: [
    "carry",
    "carry",
    "carry",
    "carry",
    "carry",
    "move",
    "move",
    "move",
    "work",
    "work",
    "work",
    "work",
  ],
  builder: [
    "carry",
    "carry",
    "carry",
    "carry",
    "carry",
    "carry",
    "move",
    "move",
    "work",
    "work",
    "work",
    "work",
  ],
  harvester: ["carry", "move", "work", "work"], // 50 + 50 + 100 + 100 = 300 Energy
  maintainer: ["carry", "carry", "carry", "move", "work", "work"],
  advancedMaintainer: [
    "carry",
    "carry",
    "carry",
    "carry",
    "carry",
    "move",
    "move",
    "move",
    "work",
    "work",
    "work",
    "work",
    "work",
  ],
};

if (!Creep.prototype.prettyMoveTo) {
  // @ts-ignore
  Creep.prototype.prettyMoveTo = function(
    target: RoomPosition | { pos: RoomPosition },
    opts?: MoveToOpts,
  ): CreepMoveReturnCode | ERR_NO_PATH | ERR_INVALID_TARGET | ERR_NOT_FOUND {
    const ret = this.moveTo(target, opts);

    switch (ret) {
      case OK:
        break;
      case ERR_TIRED:
        this.room.visual.circle(this.pos, {
          fill: "cyan",
          radius: 0.02 * this.fatigue,
        });
        break;
      case ERR_NO_PATH:
        drawHex(this.room.visual, this.pos);
        break;
      default:
        this.room.visual
          // @ts-ignore
          .poly(Room.deserializePath(this.memory._move.path), {
            fill: "transparent",
            lineStyle: "dashed",
            opacity: 0.4,
            stroke: "red",
            strokeWidth: 0.15,
          })
          .circle(this.pos, {
            fill: "red",
            radius: 0.2,
          })
          .text(ret, this.pos, { stroke: "black", font: 0.7 });
    }

    return ret;
  };
}

// When compiling TS to JS and bundling with rollup, the line numbers and file names in error messages change
// This utility uses source maps to get the line numbers and file names of the original, TS source code
export const loop = ErrorMapper.wrapLoop(() => {
  // Automatically delete memory of missing creeps
  for (const name in Memory.creeps) {
    if (!(name in Game.creeps)) {
      delete Memory.creeps[name];
    }
  }

  for (const roomStr in Game.rooms) {
    const room = Game.rooms[roomStr];

    NEEDED_ROLES.forEach((amount, role) => {
      const currentCreeps = room.find(FIND_MY_CREEPS, {
        filter: creep => {
          // tslint:disable-next-line: no-unused-expression
          return creep.memory.role.name === role;
        },
      }).length;
      if (amount > currentCreeps) {
        for (const spawn of room.find(FIND_MY_SPAWNS)) {
          let index;
          try {
            index = room.memory.roleIndex;
          } catch (error) {
            room.memory.roleIndex = 1;
            index = 1;
          }

          const sources = room.find(FIND_SOURCES);
          let source;
          try {
            source = sources[sources.length % index].id;
          } catch (err) {
            source = null;
          }

          const result = spawn.spawnCreep(
            CREEP_TEMPLATES[role],
            `${room.name}-${role}-${index}`,
            {
              memory: {
                role: {
                  index,
                  name: role,
                },
                source,
              },
            },
          );

          if (result === OK) {
            console.log(
              `Spawning Creep ${role} with index ${index} in room ${room.name}`,
            );
            room.memory.roleIndex += 1;
          }
        }
      }
    });
  }
  /*
  spawnloop: for (const roomStr in Game.rooms) {
    const room = Game.rooms[roomStr];

    for (const spawn of room.find(FIND_MY_SPAWNS, {
      filter: foundSpawn => {
        // tslint:disable-next-line: no-unused-expression
        !foundSpawn.spawning;
      },
    })) {
      const role = room.memory.spawnQueue.pop();
      if (!role) {
        break spawnloop;
      }
      const index = room.memory.roleIndex;
      spawn.spawnCreep(CREEP_TEMPLATES[role], `${role}${index}`, {
        memory: {
          role: {
            name: role,
            index: index,
          },
        },
      });
      room.memory.roleIndex += 1;
    }
  }
  */
  for (const name in Game.creeps) {
    const creep = Game.creeps[name];
    switch (creep.memory.role.name) {
      case "builder":
        roleBuilder(creep);
        break;

      case "harvester":
      case "advancedHarvester":
        roleHarvester(creep);
        break;

      case "maintainer":
      case "advancedMaintainer":
        roleMaintainer(creep);
        break;

      default:
        break;
    }
  }
});
