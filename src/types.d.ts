// example declaration file - remove these and add your own custom typings

// memory extension samples
interface CreepMemory {
  role: {
    name: string;
    index: number;
  };
  source?: string;
  working?: boolean;
}

interface RoomMemory {
  roleIndex: number;
  spawnQueue: Array<string>;
}

interface NeededRoles {
  [roleName: string]: number;
}

interface CreepTemplates {
  [roleName: string]: BodyPartConstant[];
}

interface Memory {
  uuid: number;
  log: any;
}

interface Creep {
  prettyMoveTo(
    x: number,
    y: number,
    opts?: MoveToOpts,
  ): CreepMoveReturnCode | ERR_NO_PATH | ERR_INVALID_TARGET;
  prettyMoveTo(
    target: RoomPosition | { pos: RoomPosition },
    opts?: MoveToOpts,
  ): CreepMoveReturnCode | ERR_NO_PATH | ERR_INVALID_TARGET | ERR_NOT_FOUND;
}

// `global` extension samples
declare namespace NodeJS {
  interface Global {
    log: any;
  }
}
