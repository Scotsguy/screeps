export const distributeResource = (creep: Creep, resource: ResourceConstant): void => {
  const target = creep.pos.findClosestByRange(FIND_MY_STRUCTURES, {
    filter: struct => {
      // tslint:disable-next-line: no-unused-expression
      const type = struct.structureType;
      return (
        (struct as any).store != null &&
        (type === "extension" || type === "spawn") &&
        (struct as any).store.getFreeCapacity(resource) > 0
      );
    },
  });

  if (target == null) {
    return;
  }

  if (creep.transfer(target, resource) === ERR_NOT_IN_RANGE) {
    creep.prettyMoveTo(target);
  }
};

export const getResource = (creep: Creep, resource: ResourceConstant): void => {
  const droppedResources = creep.pos.findClosestByPath(FIND_DROPPED_RESOURCES, {
    filter: dropped => {
      // tslint:disable-next-line: no-unused-expression
      dropped.resourceType === resource;
    },
  });
  if (droppedResources) {
    if (creep.pickup(droppedResources) === ERR_NOT_IN_RANGE) {
      creep.prettyMoveTo(droppedResources);
    }
  } else {
    const grave = creep.pos.findClosestByPath(FIND_TOMBSTONES, {
      filter: struct => {
        return (
          (struct as any).store != null &&
          (struct as any).store.getUsedCapacity(resource) > 0
        );
      },
    });

    if (grave) {
      if (creep.withdraw(grave, resource) === ERR_NOT_IN_RANGE) {
        creep.prettyMoveTo(grave);
      }
    }
  }

  let source: Source;

  if (creep.memory.source) {
    // @ts-ignore
    source = Game.getObjectById(creep.memory.source)!;
  } else {
    const available = creep.room.find(FIND_SOURCES);
    const index = creep.memory.role.index;

    source = available[available.length % index];
  }

  if (creep.harvest(source) === ERR_NOT_IN_RANGE) {
    creep.prettyMoveTo(source);
  }
};

export const drawHex = (
  visual: RoomVisual,
  center: RoomPosition,
  style?: PolyStyle,
): RoomVisual => {
  // http://www.rdwarf.com/lerickson/hex/

  const C = 1;
  const A = C / 2;
  const B = C * Math.sin(60 * (Math.PI / 180));

  const path: Array<[number, number]> = [
    [0, A + C],
    [0, A],
    [B, 0],
    [2 * B, A],
    [2 * B, A + C],
    [B, 2 * C],
    // Repeat from the top
    [0, A + C],
    [0, A],
  ];

  const SCALE_FACTOR = 0.8;

  path.forEach(point => {
    point[0] *= SCALE_FACTOR;
    point[1] *= SCALE_FACTOR;
  });

  path.forEach(point => {
    point[0] += center.x - 0.9 * SCALE_FACTOR;
    point[1] += center.y - 1 * SCALE_FACTOR; // Positive: Downwards
  });

  const drawOpts = style || { stroke: "magenta" };

  return visual.poly(path, drawOpts);
};
