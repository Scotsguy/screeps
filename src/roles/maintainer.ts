import { getResource } from "../utils";

export default (creep: Creep): void => {
  if (creep.memory.working) {
    const controller = creep.room.find(FIND_MY_STRUCTURES).find(struct => {
      return struct instanceof StructureController;
    }) as StructureController;
    if (creep.upgradeController(controller) === ERR_NOT_IN_RANGE) {
      creep.prettyMoveTo(controller);
    }
    if (creep.carry.energy === 0) {
      creep.memory.working = false;
    }
  } else {
    if (creep.carry.energy < creep.carryCapacity) {
      getResource(creep, RESOURCE_ENERGY);
    } else {
      creep.memory.working = true;
    }
  }
};
