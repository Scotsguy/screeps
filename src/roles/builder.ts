import { getResource } from "../utils";

export default (creep: Creep): void => {
  if (creep.memory.working) {
    const target = creep.pos.findClosestByRange(FIND_CONSTRUCTION_SITES);
    if (target) {
      if (creep.build(target) === ERR_NOT_IN_RANGE) {
        creep.prettyMoveTo(target);
      }
    } else {
      const targets = creep.room.find(FIND_STRUCTURES, {
        filter: object => object.hits < object.hitsMax,
      });

      if (targets.length > 0) {
        targets.sort((a, b) => a.hits - b.hits);
        if (creep.repair(targets[0]) === ERR_NOT_IN_RANGE) {
          creep.prettyMoveTo(targets[0]);
        }
      }
      creep.memory.working = false;
    }

    if (creep.carry.energy === 0) {
      creep.memory.working = false;
    }
  } else {
    if (creep.carry.energy < creep.carryCapacity) {
      getResource(creep, RESOURCE_ENERGY);
    } else {
      creep.memory.working = true;
    }
  }
};
