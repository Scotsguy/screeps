import { distributeResource } from "utils";

export default (creep: Creep): void => {
  if (creep.carry.energy < creep.carryCapacity) {
    const sources = creep.room.find(FIND_SOURCES);
    const index = creep.memory.role.index;
    if (creep.harvest(sources[index % sources.length]) === ERR_NOT_IN_RANGE) {
      creep.prettyMoveTo(sources[index % sources.length]);
    }
  } else {
    distributeResource(creep, RESOURCE_ENERGY);
  }
};
